#include "queue.h"

int main()
{
	queue* q = new queue;
	initQueue(q, 3);
	enqueue(q, 3);
	enqueue(q, 7);
	enqueue(q, 4);
	enqueue(q, 4);
	printQueue(q); // will print all the arguments
	std::cout << dequeue(q) << std::endl; // prints the deletd arguments
	printQueue(q); // will print all the arguments
	cleanQueue(q);
	delete(q);
	getchar();
	return 0;
}